
// Variable globale contenant l'état du lecteur
let etatLecteur;

function lecteurPret(event) {
    // event.target = lecteur
    event.target.setVolume(50);
}

function changementLecteur(event) {
    // event.data = état du lecteur
    etatLecteur = event.data;
}

let lecteur;

function onYouTubeIframeAPIReady() {
    lecteur = new YT.Player("video", {
        height: "390",
        width: "640",
        videoId: "n9xhJrPXop4",
        playerVars: {
            color: "white",
            enablejsapi: 1,
            modestbranding: 1,
            rel: 0
        },
        events: {
            onReady: lecteurPret,
            onStateChange: changementLecteur
        }
    });
}

// Hauteur de la vidéo
const hauteurVideo = $("#video").height();
// Position Y de la vidéo
const posYVideo = $("#video").offset().top;
// Valeur declenchant la modification de l'affichage (choix "esthétique")
const seuil = posYVideo + 0.75 * hauteurVideo;

// Gestion du défilement
$(window).scroll(function () {
    // Récupération de la valeur du défilement vertical
    const scroll = $(window).scrollTop();

    // Classe permettant l'exécution du CSS
    $("#video").toggleClass(
        "scroll",
        etatLecteur === YT.PlayerState.PLAYING && scroll > seuil
    );
});

//============================================================ CARROUSEL=======================================================//




// Variable globale
let index = 0;
$('.character>.desc>p').text($('.character>.carou>img').eq(index).attr('alt'));

// Fonction pour passer à la diapositive suivante
function nextSlide() {
    let indexN = index + 1;
    if (indexN > $('.character>.carou>img').length - 1) {
        indexN = 0;
    }

    // Renouveler l'image
    $('.character>.carou>img').eq(index).fadeOut(1000).end().eq(indexN).fadeIn(1000);
    $('.character>.desc>p').text($('.character>.carou>img').eq(indexN).attr('alt'));

    index = indexN;
}

// Gestion automatique du carousel
const interval = setInterval(nextSlide, 2500); // Change de slide toutes les 2 secondes (modifiable)


// Gestion des événements pour le bouton suivant
$('.arrowR').click(function () {
    nextSlide();
});

// Gestion des événements pour le bouton précédent
$('.arrowL').click(function () {
    let indexN = index - 1;
    if (indexN < 0) {
        indexN = $('.character>.carou>img').length - 1;
    }

    // Renouveler l'image
    $('.character>.carou>img').eq(index).fadeOut(1000).end().eq(indexN).fadeIn(1000);
    $('.character>.desc>p').text($('.character>.carou>img').eq(indexN).attr('alt'));

    index = indexN;
});

//========================================================== CARROUSEL 2  ====================================================//

// Variable globale
let index2 = 0;
$('.character2>.desc2>p').text($('.character2>.carou2>img').eq(index2).attr('alt'));

// Fonction pour passer à la diapositive suivante
function nextSlide2() {
    let indexN2 = index2 + 1;
    if (indexN2 > $('.character2>.carou2>img').length - 1) {
        indexN2 = 0;
    }

    // Renouveler l'image
    $('.character2>.carou2>img').eq(index2).fadeOut(1000).end().eq(indexN2).fadeIn(1000);
    $('.character2>.desc2>p').text($('.character2>.carou2>img').eq(indexN2).attr('alt'));

    index2 = indexN2;
}

// Gestion automatique du carousel
const interval2 = setInterval(nextSlide2, 2500); // Change de slide toutes les 2 secondes (modifiable)


// Gestion des événements pour le bouton suivant
$('.arrowR2').click(function () {
    nextSlide2();
});

// Gestion des événements pour le bouton précédent
$('.arrowL2').click(function () {
    let indexN2 = index2 - 1;
    if (indexN2 < 0) {
        indexN2 = $('.character2>.carou2>img').length - 1;
    }

    // Renouveler l'image
    $('.character2>.carou2>img').eq(index2).fadeOut(1000).end().eq(indexN2).fadeIn(1000);
    $('.character2>.desc2>p').text($('.character2>.carou2>img').eq(indexN2).attr('alt'));

    index2 = indexN2;
});


//==========================================================  MAP ====================================================//


// var map = L.map('map').setView([48.8566, 2.3522], 13);



// L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
//     maxZoom: 19,
//     attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
// }).addTo(map);

// var marker = L.marker([51.5, -0.09]).addTo(map);

// var circle = L.circle([51.508, -0.11], {
//     color: 'red',
//     fillColor: '#f03',
//     fillOpacity: 0.5,
//     radius: 500
// }).addTo(map);

// var polygon = L.polygon([
//     [51.509, -0.08],
//     [51.503, -0.06],
//     [51.51, -0.047]
// ]).addTo(map);

// // marker.bindPopup("<b>Hello world!</b><br>I am a popup.").openPopup();
// // circle.bindPopup("I am a circle.");
// // polygon.bindPopup("I am a polygon.");

// // var popup = L.popup()
// //     .setLatLng([51.513, -0.09])
// //     .setContent("I am a standalone popup.")
// //     .openOn(map);

// //     function onMapClick(e) {
// //         alert("You clicked the map at " + e.latlng);
// //     }
    
// //     map.on('click', onMapClick);
    
// //     var popup = L.popup();

// //     function onMapClick(e) {
// //         popup
// //             .setLatLng(e.latlng)
// //             .setContent("You clicked the map at " + e.latlng.toString())
// //             .openOn(map);
// //     }
    
// //     map.on('click', onMapClick);   


// var geojsonFeature = {
//     "type": "Feature",
//     "properties": {
//         "name": "Coors Field",
//         "amenity": "Baseball Stadium",
//         "popupContent": "This is where the Rockies play!"
//     },
//     "geometry": {
//         "type": "Point",
//         "coordinates": [-104.99404, 39.75621]
//     }
// };

// L.geoJSON(geojsonFeature).addTo(map);


// var myLines = [{
//     "type": "LineString",
//     "coordinates": [[-100, 40], [-105, 45], [-110, 55]]
// }, {
//     "type": "LineString",
//     "coordinates": [[-105, 40], [-110, 45], [-115, 55]]
// }];

// var myLayer = L.geoJSON().addTo(map);
// myLayer.addData(geojsonFeature);

// var myLines = [{
//     "type": "LineString",
//     "coordinates": [[-100, 40], [-105, 45], [-110, 55]]
// }, {
//     "type": "LineString",
//     "coordinates": [[-105, 40], [-110, 45], [-115, 55]]
// }];

// var myStyle = {
//     "color": "#ff7800",
//     "weight": 5,
//     "opacity": 0.65
// };

// L.geoJSON(myLines, {
//     style: myStyle
// }).addTo(map);

// var states = [{
//     "type": "Feature",
//     "properties": {"party": "Republican"},
//     "geometry": {
//         "type": "Polygon",
//         "coordinates": [[
//             [-104.05, 48.99],
//             [-97.22,  48.98],
//             [-96.58,  45.94],
//             [-104.03, 45.94],
//             [-104.05, 48.99]
//         ]]
//     }
// }, {
//     "type": "Feature",
//     "properties": {"party": "Democrat"},
//     "geometry": {
//         "type": "Polygon",
//         "coordinates": [[
//             [-109.05, 41.00],
//             [-102.06, 40.99],
//             [-102.03, 36.99],
//             [-109.04, 36.99],
//             [-109.05, 41.00]
//         ]]
//     }
// }];

// L.geoJSON(states, {
//     style: function(feature) {
//         switch (feature.properties.party) {
//             case 'Republican': return {color: "#ff0000"};
//             case 'Democrat':   return {color: "#0000ff"};
//         }
//     }
// }).addTo(map);

// var geojsonMarkerOptions = {
//     radius: 8,
//     fillColor: "#ff7800",
//     color: "#000",
//     weight: 1,
//     opacity: 1,
//     fillOpacity: 0.8
// };

// L.geoJSON(someGeojsonFeature, {
//     pointToLayer: function (feature, latlng) {
//         return L.circleMarker(latlng, geojsonMarkerOptions);
//     }
// }).addTo(map);

// function onEachFeature(feature, layer) {
//     // does this feature have a property named popupContent?
//     if (feature.properties && feature.properties.popupContent) {
//         layer.bindPopup(feature.properties.popupContent);
//     }
// }

// var geojsonFeature = {
//     "type": "Feature",
//     "properties": {
//         "name": "Coors Field",
//         "amenity": "Baseball Stadium",
//         "popupContent": "This is where the Rockies play!"
//     },
//     "geometry": {
//         "type": "Point",
//         "coordinates": [-104.99404, 39.75621]
//     }
// };

// L.geoJSON(geojsonFeature, {
//     onEachFeature: onEachFeature
// }).addTo(map);

// var someFeatures = [{
//     "type": "Feature",
//     "properties": {
//         "name": "Coors Field",
//         "show_on_map": true
//     },
//     "geometry": {
//         "type": "Point",
//         "coordinates": [-104.99404, 39.75621]
//     }
// }, {
//     "type": "Feature",
//     "properties": {
//         "name": "Busch Field",
//         "show_on_map": false
//     },
//     "geometry": {
//         "type": "Point",
//         "coordinates": [-104.98404, 39.74621]
//     }
// }];

// L.geoJSON(someFeatures, {
//     filter: function(feature, layer) {
//         return feature.properties.show_on_map;
//     }
// }).addTo(map); 





// var map = L.map('map').setView([47.3907, 5.0624], 13);
// L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
//     attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
// }).addTo(map);
// map.locate({setView: true, maxZoom: 16});


// map.on('locationfound', function(e) {
//     L.marker(e.latlng).addTo(map);
// });

// function onEachFeature(feature, layer) {
//     if (feature.properties && feature.properties.name) {
//         layer.bindPopup(feature.properties.name);
//     }
// }

// L.geoJson(data, {
//     onEachFeature: onEachFeature
// }).addTo(map);

// map.on('locationerror', function() {
//     alert('Erreur de géolocalisation. Veuillez activer la géolocalisation sur votre appareil.');
// });






var map = L.map('map').setView([47.3907, 5.0624], 13); // Localisation par défaut

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '© OpenStreetMap contributors'
    }).addTo(map);

    navigator.geolocation.getCurrentPosition(function(location) {
        var latlng = new L.LatLng(location.coords.latitude, location.coords.longitude);
        map.setView(latlng);
        L.marker(latlng).addTo(map);
    });

   // Ajoutez les emplacements des cinémas manuellement ou récupérez-les à partir d'une API
   var cinemas = [
       [47.3907, 5.0624],
       // Ajoutez plus de coordonnées si nécessaire
   ];

   cinemas.forEach(function(coords) {
       L.marker(coords).addTo(map).bindPopup('Cinéma: CINE CAP VERT, 12 salles');
   });
